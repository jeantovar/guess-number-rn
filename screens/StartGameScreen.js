import React, { useState } from 'react';
import { View, Text, 
        StyleSheet, Button, 
        TouchableWithoutFeedback, 
        Keyboard, Alert } from 'react-native';
import Card from '../components/Card';
import Colors from '../constants/colors'
import Input from '../components/Input';
import NumberContainer from '../components/NumberContainer';
import colors from '../constants/colors';
import TitleText from '../components/TitleText';
import BodyText from '../components/BodyText';
import AppButton from '../components/AppButton';

const StartGameScreen = props => {

    const [enteredValue, setEnteredValue] = useState('');

    const [confirmed, setConfirmed] = useState(false);

    const [selectedNumber, setSelectedNumber] = useState();

    const validNumberHandler = inputText => {
        setEnteredValue(inputText.replace(/[^0-9]/g,''));
    }

    const resetNumberHandler = () => {
        setEnteredValue('');
        setConfirmed(false);
    }

    const confirmNumberHandler = () => {
        const choosenNumber = Number.parseInt(enteredValue);
        if(isNaN(choosenNumber)  || choosenNumber <= 0 || choosenNumber > 99){
            Alert.alert('Invalid Number!', 
                        "Should be a number between 1 and 99",
                        [{text:'Okay', style: "destructive", onPress: resetNumberHandler}])
            return;
        }
        setConfirmed(true);
        setSelectedNumber(choosenNumber);
        setEnteredValue('');
        Keyboard.dismiss();
        
    }

    let confirmedOutputText;

    if(confirmed){
        confirmedOutputText = (
                <Card style={styles.summaryCard}>
                    <BodyText>You selected</BodyText>
                    <NumberContainer>{selectedNumber}</NumberContainer>
                    <AppButton onPress={()=> props.onStartGame(selectedNumber)}>
                        START GAME
                    </AppButton>
                </Card>
        )

    }
    return (
        <TouchableWithoutFeedback onPress={()=>{
            Keyboard.dismiss();
        }}>
            <View style={styles.screen}>
                <TitleText style={styles.title}>Start a new Game!</TitleText>
                <Card style={styles.inputBox}>
                    <Input placeholder="Select a Number"
                        style={styles.input}
                        keyboardType="number-pad" maxLength={2} 
                        onChangeText={validNumberHandler}
                        value={enteredValue}
                        />
                    <View style={styles.buttonBox}>
                            <AppButton onPress={ confirmNumberHandler }>Confirm</AppButton>
                            
                       <View style={styles.button}>
                            <AppButton onPress={ resetNumberHandler } 
                                        style={styles.button2}>Reset</AppButton>
                                
                        </View>

                    </View>
                </Card>
                {confirmedOutputText}
            </View>
        </TouchableWithoutFeedback>
        
    );
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        alignItems: "center",
        //justifyContent: "center",
        padding: 10
    },
    buttonBox: {
        flexDirection: "row-reverse",
        width: "100%",
        paddingHorizontal: 20,
        justifyContent: 'space-between',
        alignItems: "center"
    },
    title: {
        fontSize: 20,
        marginVertical: 10
        
    },
    inputBox: {
        width: "80%",
        alignItems: "center",

    },
    button2: {
        backgroundColor: Colors.secondary
    },
    input: {
        width: 140,
        textAlign: "center"
    },
    summaryCard: {
        marginTop: 20,
        alignItems: "center"
    }
});

export default StartGameScreen;