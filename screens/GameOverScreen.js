import React from 'react';

import {View, Text, StyleSheet, Button, Image} from 'react-native';
import BodyText from '../components/BodyText';
import TitleText from '../components/TitleText';
import  Colors  from '../constants/colors';
import AppButton from '../components/AppButton';

const GameOverScreen = props => {
    return (
        <View style={styles.screen}>
            <TitleText>Game Over!</TitleText>
            <View style={styles.imageContainer}>
                <Image 
                    source={require('../assets/success.png')}
                    //source={{uri: 'https://ichef.bbci.co.uk/news/660/cpsprodpb/10869/production/_107098676_1c45dac3-bf7e-4663-bdec-099ec9d8e199.jpg'} }
                    style={styles.image}
                    resizeMode="cover"
                />
            </View>
            <View style={styles.summary}>
            <BodyText style={styles.summaryText}>Your phone needed <Text style={styles.highlight}>{props.rounds}</Text> rounds to guess the number <Text style={styles.highlight}>{props.userNumber}</Text></BodyText>
            </View>
            
            <AppButton onPress={props.onPlayAgain}
                        style={styles.button2}>NEW GAME</AppButton>
        </View>
    );
}

const styles = StyleSheet.create({
    screen: {
        flex : 1,
        justifyContent: "center",
        alignItems: "center"
    },
    imageContainer:{
        width: 300,
        height: 300,
        borderRadius: 150,
        borderWidth: 3,
        borderColor: Colors.primary,
        overflow: 'hidden',
        marginVertical: 30
    },
    image: {
        width: '100%',
        height: '100%'
    },
    highlight: {
        color: Colors.secondary,
        fontFamily: 'open-sans-bold'
    },
    summary:{
        marginVertical: 15,
        marginHorizontal: 20
    },
    summaryText:{
        textAlign: "center",
        fontSize: 20
    },
    button2:{
        backgroundColor: Colors.secondary
    }

});

export default GameOverScreen;