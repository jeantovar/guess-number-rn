import React, { useState, useRef, useEffect } from 'react';
import { View, Text, StyleSheet, ScrollView, Alert } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import NumberContainer from '../components/NumberContainer';
import Card from '../components/Card';
import AppButton from '../components/AppButton';

const generateRandomNumber = (min, max, exclude) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    const rndNumber = Math.floor(Math.random() * (max - min)) + min;
    if (rndNumber === exclude) {
        return generateRandomNumber(min, max, exclude);
    } else {
        return rndNumber;
    }
}

const GameScreen = props => {

    const initialGuess = generateRandomNumber(1, 100, props.userChoice);
    const [guessNumber, setGuessNumber] = useState(initialGuess);
    const [pastGuesses, setPastGuesses] = useState([initialGuess]);
    const [rounds, setRounds] = useState(0);
    const currentLow = useRef(1);
    const currentMax = useRef(100);

    const { userChoice, onGameOver } = props;

    useEffect(() => {
        if (guessNumber === userChoice) {
            onGameOver(pastGuesses.length);
        }

    }, [guessNumber, userChoice, onGameOver]);

    const nextGuessHandler = direction => {
        if (
            (direction === 'lower' && guessNumber < props.userChoice) ||
            (direction === 'greater' && guessNumber > props.userChoice)
        ) {
            Alert.alert("Don\'t lie!", "You know that this is wrong..", [{ text: "Sorry", style: "cancel" }]);
            return;
        }

        if (direction === 'lower') {
            currentMax.current = guessNumber;
        } else {
            currentLow.current = guessNumber + 1;
        }

        nextNumber = generateRandomNumber(currentLow.current, currentMax.current, guessNumber);
        setGuessNumber(nextNumber);
        setPastGuesses(curPast => [nextNumber , ...curPast]);
    }

    return (
        <View style={styles.screen}>
            <Text>Opponent's Number</Text>
            <NumberContainer>{guessNumber}</NumberContainer>
            <Card style={styles.buttonContainer}>
                <AppButton  onPress={nextGuessHandler.bind(this, 'lower')} >
                    <Ionicons name="md-remove" size={24}  color="white"/>
                </AppButton>
                <AppButton onPress={nextGuessHandler.bind(this, 'greater')}>
                    <Ionicons name="md-add" size={24}  color="white"/>
                </AppButton>
            </Card>
            <ScrollView>
                {
                    pastGuesses.map(guess =>{
                        return (<View key={guess}>
                                    <Text>{guess}</Text>
                                </View>);
                    })
                }
                
            </ScrollView>
        </View>
        
        
    );
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        padding: 10,
        alignItems: "center"
    },
    buttonContainer: {
        flexDirection: "row",
        marginTop: 20,
        justifyContent: "space-around",
        width: 300,
        maxWidth: "80%"
    }
});

export default GameScreen;