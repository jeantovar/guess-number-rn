import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import colors from '../constants/colors';

const AppButton = props => {
    return (
        <TouchableOpacity onPress={props.onPress}>
            <View style={{...styles.button, ...props.style}}>
               <Text style={styles.buttonTxt}>{props.children}</Text> 
            </View>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    button:{
        borderRadius: 20,
        backgroundColor: colors.primary,
        paddingVertical: 12,
        paddingHorizontal: 20,
        alignItems: "center",
        minWidth: 100 
        
    },

    buttonTxt:{
        color: 'white',
        fontFamily: 'open-sans',
        fontSize: 18
    }
});

export default AppButton;
