import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import colors from '../constants/colors';
import BodyText from './BodyText';

const NumberContainer = props => {
    return (
        <View style={styles.container}>
            <BodyText style={styles.number}>{props.children}</BodyText>
        </View>
    );
}

const styles = StyleSheet.create({
    number:{
        fontSize: 22,
        color: colors.secondary
    },
    container:{
        justifyContent: "center",
        alignContent: "center",
        borderWidth: 2,
        borderColor: colors.secondary,
        padding: 10,
        borderRadius: 10,
        marginVertical: 10


    }
});

export default NumberContainer;